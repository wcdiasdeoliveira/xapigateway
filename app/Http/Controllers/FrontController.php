<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    private function prepareCurlGetRequest($token, $resource_id){
      $ch = curl_init();
      $params = http_build_query(["token" => $token, "resource_id" => $resource_id]);
      $urlPlusGetParams = "http://localhost:8002?".$params;
      curl_setopt($ch,CURLOPT_URL,$urlPlusGetParams);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      return $ch;
    }

    public function getResourceN(Request $request){
      try {
        $resource_id = $request->input("resource_id");
        $token = $request->input("token");
        $authorizedTokens = ["çlkzxc"];
        $authorizedResourcesForToken = [$authorizedTokens[0] => ["1010"]];
        if(in_array($token, $authorizedTokens)){
          if(in_array($resource_id, $authorizedResourcesForToken[$token])){
            $ch = $this->prepareCurlGetRequest($token, $resource_id);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
          }else{
            return json_encode(["message" => "You're not authorized to access this resource."]);
          }
        }else{
          return json_encode(["message" => "You're not a authorized user."]);
        }
      } catch (\Exception $e) {
        return json_encode(["message" => "Failed to proccess request."]);
      }
    }
}
